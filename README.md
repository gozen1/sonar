Welcome to our intermediate to advanced level backend engineer technical test. The goal is to get to know a little bit of your technical background and expertise. 

It comprises two areas:

- Answering some technical questions
- Code refactor exercise

There is no time limit, nor a minimum number of answers to accomplish. As mentioned before, the aim is to get a feeling of your technical profile, and be able to refer to your answers and code in further discussions.

# Technical questions

Please refer to [QUESTIONS.md](QUESTIONS.md).

## Instructions

1. A user was already created for you inside Caravelo's GitLab
2. refactor-1 repo has already been forked into this space `recruitment/candidates/${your-username}`
3. Edit QUESTIONS.md and add your answers
4. Commit & push your changes to this repository
5. Notify to Caravelo's HR contact when done

# Refactor 1: Flow Executor

[![Level](https://img.shields.io/badge/Level-intermediate-blue.svg)](https://shields.io/)

This is Caravelo's refactoring assignment from intermediate to advanced level.

In this repository you will find low quality code, full of bad smells, with poor packaging, modeling, design &c.

We would like you to refactor this code having the following drivers in mind:
- Readability and conceptual integrity
- Maintainability (i.e. modifying or extending existing functionality and fixing bugs)
- Evolvability (i.e. adding new flows)
- Clarity and coherence

You can start with the unit test located in 'src/test' to figure out the expected behaviour. 

Don't pay much attention to semantics of the project since it's a fake use case.

## Prerequisites

* Java 8
* [Gradle](https://gradle.org/)

## Instructions

1. A user was already created for you inside Caravelo's GitLab
2. refactor-1 repo has already been forked into this space `recruitment/candidates/${your-username}`
3. Commit & push your changes to this repository
4. Notify to Caravelo's HR contact when done

---

( ^..^)ﾉ Have fun!
