package refactor.execute;

import refactor.bus.Bus;
import refactor.model.Campaign;
import refactor.model.Flight;
import refactor.model.Outcome;

public class CommandB implements Command {

    protected static final int LONG_HAUL_SAFETY_MARGIN = 40;
    protected static final int SHORT_HAUL_SAFETY_MARGIN = 10;
    protected final Campaign campaign;
    protected final Flight flight;

    public CommandB(Campaign campaign, Flight flight) {
        this.campaign = campaign;
        this.flight = flight;
    }

    @Override
    public Outcome execute(Bus bus) {
        Outcome outcome = new Outcome();
        final int target = determineRelativeTarget();
        outcome.setTarget(target);
        final int safetyMargin = flight.isLongHaul() ? LONG_HAUL_SAFETY_MARGIN : SHORT_HAUL_SAFETY_MARGIN;
        final int netCapacity = flight.getCapacity() - safetyMargin;
        final int netTarget = target - flight.getCapacity();
        final int totalTarget = flight.getSold() + netTarget;
        final boolean shouldFlightOpened = netCapacity >= totalTarget;
        if (shouldFlightOpened) {
            outcome.setInventoryStatus(Outcome.FlightInventoryStatus.OPEN);
        } else {
            outcome.setInventoryStatus(Outcome.FlightInventoryStatus.CLOSED);
            bus.post(Events.toReview(flight));
        }
        return outcome;
    }

    protected int determineRelativeTarget() {
        return flight.getCapacity() + (int) (flight.getCapacity() * campaign.getOversellFactor());
    }
}
