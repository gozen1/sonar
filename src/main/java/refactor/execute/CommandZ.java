package refactor.execute;

import refactor.bus.Bus;
import refactor.model.Outcome;
import refactor.exception.UnsupportedFlowType;

public class CommandZ implements Command {

    @Override
    public Outcome execute(Bus bus) {
        throw new UnsupportedFlowType();
    }
}
