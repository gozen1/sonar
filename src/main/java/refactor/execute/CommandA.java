package refactor.execute;

import refactor.bus.Bus;
import refactor.model.Campaign;
import refactor.model.Flight;
import refactor.model.Outcome;

public class CommandA implements Command {

    protected final Campaign campaign;
    protected final Flight flight;
    protected final String email;

    public CommandA(Campaign campaign, Flight flight, String email) {
        this.campaign = campaign;
        this.flight = flight;
        this.email = email;
    }

    @Override
    public Outcome execute(Bus bus) {
        Outcome outcome = new Outcome();
        final int target = determineAbsoluteTarget();
        outcome.setTarget(target);
        final boolean shouldFlightClosed = flight.getSold() >= flight.getCapacity();
        if (shouldFlightClosed) {
            outcome.setInventoryStatus(Outcome.FlightInventoryStatus.CLOSED);
        } else {
            outcome.setInventoryStatus(Outcome.FlightInventoryStatus.OPEN);
        }
        bus.post(Events.notify(email, outcome));
        return outcome;
    }

    private int determineAbsoluteTarget() {
        return flight.getCapacity() + campaign.getOversell();
    }
}
