package refactor.execute;

import refactor.bus.Bus;
import refactor.bus.CampaignBus;
import refactor.model.Outcome;

/**
 * The smelly flow executor.
 */
public class FlowExecutor {

    private Bus bus;

    public FlowExecutor() {
        this.bus = new CampaignBus();
    }

    /**
     * Executes a flow depending on the given parameters.
     *
     * @param c The command to be run
     * @return the execution outcome
     */
    public Outcome execute(Command c) {
        return c.execute(bus);
    }

    void register(Object listener) {
        this.bus.register(listener);
    }
}
