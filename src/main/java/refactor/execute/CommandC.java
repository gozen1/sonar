package refactor.execute;

import refactor.bus.Bus;
import refactor.model.Flight;
import refactor.model.Outcome;

public class CommandC implements Command {

    protected final Flight flight;

    public CommandC(Flight flight) {
        this.flight = flight;
    }

    @Override
    public Outcome execute(Bus bus) {
        Outcome outcome = new Outcome();
        final float loadFactor = flight.getSold().floatValue() / flight.getCapacity();
        final boolean flightHasEnoughCapacity = loadFactor > 1;
        if (flightHasEnoughCapacity) {
            outcome.setTarget(flight.getCapacity());
        } else {
            bus.post(Events.toCleaningQueue(flight));
        }
        return outcome;
    }
}
