package refactor.execute;

import refactor.bus.Bus;
import refactor.model.Outcome;

public interface Command {
    Outcome execute(Bus bus);
}
