package refactor.bus;

import com.google.common.eventbus.EventBus;

public class CampaignBus implements Bus {
    private final EventBus bus;

    public CampaignBus() {
        this.bus = new EventBus();
    }

    @Override
    public void post(Object event) {
        this.bus.post(event);
    }

    @Override
    public void register(Object listener) {
        this.bus.register(listener);
    }
}
