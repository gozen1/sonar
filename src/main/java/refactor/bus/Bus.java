package refactor.bus;

public interface Bus {
    void post(Object event);
    void register(Object listener);
}
